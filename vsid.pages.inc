<?php

/**
 * @file
 * Config-page functions.
 */


function _vsid__configpage($form, &$form_state) {
  $example_sites = array('' => "#e2e200", 'abc.' => 'red', 'xyz.' => 'green' );
  foreach ($example_sites as $prefix => $color) {
    $example_rows[] = sprintf('"%s%s" : "%s"', $prefix, $_SERVER['SERVER_NAME'], $color);
  }
  $orig_val = implode(",\n", $example_rows) . "\n<<Change these for your set up, and delete this row>>";

  $form['vsid_servernames'] = array(
      '#type' => 'textarea',
      '#title' => t('Server Names to change colors for'),
      '#description' => t('Format should be like this: <dl><dd>"www.mysite.com" : "red",<br/>"staging.mysite.com" : "rgba(45,34,158,0.5)",<br/>"development.mysite.com" : "#ffff00"</dd></dl>'),
      '#default_value' => variable_get('vsid_servernames', $orig_val),
  );

  if ($links = _vsid__get_links()) {
    $form['links'] = array(
        '#markup' => $links,
    );
  }
  $form = system_settings_form($form);
  $form['#submit'][] = '_vsid__configpage_submit';
  return $form;
}


function _vsid__configpage_validate($form, &$form_state) {
  $text = trim($form_state['input']['vsid_servernames']);
  $parsed_names = _vsid__parse_servernames($text);
  if (empty($parsed_names) && !empty($text)) {
    form_set_error('vsid_servernames', "Something doesn't look right.");
  }
}

function _vsid__configpage_submit($form, &$form_state) {
  if ($form['vsid_servernames']['#value'] != $form['vsid_servernames']['#default_value']) {
    drupal_set_message(_vsid__get_links(), 'warning');
  }
}

function _vsid__get_links() {
  $links = '';
  $configs = _vsid__get_configs();
  $other_servers_seen = 0;
  foreach ($configs as $server => $color) {
    if ($server == $_SERVER['SERVER_NAME']) {
      $seen_self = TRUE;
    }
    else {
      ++$other_servers_seen;
      $httpserver = 'http://' . $server;
      $links .= '<li>'
              . l("$server (VSID config page)", "$httpserver/admin/config/development/vsid")
              . '</li>';
    }
  }
  if (!$seen_self) {
    drupal_set_message(t("You should set a color for this server (@self) in the VSID settings.",
            array('@self' => $_SERVER['SERVER_NAME'])), 'warning');
  }
  if ($links)
    return t('You need to copy and paste your <em>exact same</em> config to the @otherservers in your set:',
            array('@otherservers' => format_plural($other_servers_seen, 'other server', '@count other servers'))) . "<ul>$links</ul>";
  else
    return '';
}