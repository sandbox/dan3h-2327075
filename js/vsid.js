(function ($) {
  Drupal.behaviors.vsid = {
    attach: function (context, settings) {
        var canvas = document.createElement('canvas');
        if (canvas.getContext) {
          canvas.height = canvas.width = 16; // set the size
          var ctx = canvas.getContext('2d');
          if( 'function'!=typeof(ctx.canvas.toDataURL) ) {
            alert("no toDataURL");
          }
          // TODO:  get by '#favicon'.  Currently, this requires a hack to includes/theme.inc
          //     Right now, it needs this added to the call to drupal_add_html_head_link()
          //        in template_preprocess_html().
          //
          //        ... , 'id'=>'favicon', ...
          
          var icon = document.getElementById('favicon');  
          var img = document.createElement('img');
          img.src = icon.getAttribute("href");
          img.onload = function () { // once the image has loaded
            ctx.fillStyle =  Drupal.settings.vsid.fillStyle;
            ctx.fillRect(0,0,16,16);
            ctx.drawImage(this, 0, 0);
            (newIcon = icon.cloneNode(true)).setAttribute('href',ctx.canvas.toDataURL());
            icon.parentNode.replaceChild(newIcon,icon);
          };
        }

    }
  };
})(jQuery);




